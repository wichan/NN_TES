#!/usr/bin/env python

import os, sys, argparse

import ROOT
import numpy as np
from glob import glob

from modules.utils import np_save


#-------------------------------------------------------------------------------
def MxAOD_to_npy(args):

    # Check whether the configuration is OK
    well_configured = True
    if not args.input:
        well_configured = False
        print "ERROR: You did not provide the input file name(s)."
    if not args.output:
        well_configured = False
        print "ERROR: You did not provide the output file name."
    if not well_configured:
        print "USAGE: MxAOD_to_npy.py -i <input_file> -o <output_file> [-m <max_events>] [--debug]"
        sys.exit(1)
    
    # List of input files
    input_files = []
    for path in args.input.split(','):
        input_files += glob(path)
    if not input_files:
        print "ERROR: No valid input files found."
        sys.exit(1)
    
    # Check whether RootCore has been set up
    if 'ROOTCOREDIR' not in os.environ or os.environ['ROOTCOREDIR'] == "":
        print "ERROR: You did not set up RootCore. Do 'rcSetup Base,2.5.1' (not necessarily in the current directory)."
        sys.exit(2)
    
    # Initialise sample counter and data array
    counter = 0
    counter_selected = 0
    break_all_loops = False
    data = []
    names = []  # Store the variable names in the right order for future reference
    
    # Loop over input trees and set branches addresses
    for path in input_files:
        f = ROOT.TFile.Open(path)
        t = f.Get("CollectionTree")
        
        ### NN inputs
        # pile-up related
        mu	 		        = ROOT.std.vector('double')()
        nVtxPU	        = ROOT.std.vector('int')()
        t.SetBranchAddress("TauJetsAuxDyn.mu", mu)
        t.SetBranchAddress("TauJetsAuxDyn.nVtxPU", nVtxPU)
        if not "mu" in names: names.append("mu")
        if not "nVtxPU" in names: names.append("nVtxPU")
        
        # cluster
        clusCenterLambda   = ROOT.std.vector('float')()
        clusFirstEngDens   = ROOT.std.vector('float')()
        clusSecondLambda   = ROOT.std.vector('float')()
        clusPresamplerFrac = ROOT.std.vector('float')()
        clusEMProbability  = ROOT.std.vector('float')()
        t.SetBranchAddress("TauJetsAuxDyn.ClustersMeanCenterLambda", clusCenterLambda)
        t.SetBranchAddress("TauJetsAuxDyn.ClustersMeanFirstEngDens", clusFirstEngDens)
        t.SetBranchAddress("TauJetsAuxDyn.ClustersMeanSecondLambda", clusSecondLambda)
        t.SetBranchAddress("TauJetsAuxDyn.ClustersMeanPresamplerFrac", clusPresamplerFrac)
        t.SetBranchAddress("TauJetsAuxDyn.ClustersMeanEMProbability", clusEMProbability)
        if not "clusCenterLambda" in names: names.append("clusCenterLambda")
        if not "clusFirstEngDens" in names: names.append("clusFirstEngDens")
        if not "clusSecondLambda" in names: names.append("clusSecondLambda")
        if not "clusPresamplerFrac" in names: names.append("clusPresamplerFrac")
        if not "clusEMProbability" in names: names.append("clusEMProbability")
        
        # reconstructed kinematics
        ptDetectorAxis     = ROOT.std.vector('float')()
        ptPanTauCellBased  = ROOT.std.vector('float')()
        etaPanTauCellBased = ROOT.std.vector('float')()
        t.SetBranchAddress("TauJetsAuxDyn.ptDetectorAxis", ptDetectorAxis)
        t.SetBranchAddress("TauJetsAuxDyn.ptPanTauCellBased", ptPanTauCellBased)
        t.SetBranchAddress("TauJetsAuxDyn.etaPanTauCellBased", etaPanTauCellBased)
        if not "ptDetectorAxis" in names: names.append("ptDetectorAxis")
        if not "ptPanTauCellBased" in names: names.append("ptPanTauCellBased")
        if not "etaPanTauCellBased" in names: names.append("etaPanTauCellBased")
        
        # decay mode related
        nTracks               = ROOT.std.vector('int')()
        BDTValue_1p0n_vs_1p1n = ROOT.std.vector('float')()
        BDTValue_1p1n_vs_1pXn = ROOT.std.vector('float')()
        BDTValue_3p0n_vs_3pXn = ROOT.std.vector('float')()
        PFOEngRelDiff         = ROOT.std.vector('float')()
        t.SetBranchAddress("TauJetsAuxDyn.nTracks", nTracks)
        t.SetBranchAddress("TauJetsAuxDyn.pantau_CellBasedInput_BDTValue_1p0n_vs_1p1n", BDTValue_1p0n_vs_1p1n)
        t.SetBranchAddress("TauJetsAuxDyn.pantau_CellBasedInput_BDTValue_1p1n_vs_1pXn", BDTValue_1p1n_vs_1pXn)
        t.SetBranchAddress("TauJetsAuxDyn.pantau_CellBasedInput_BDTValue_3p0n_vs_3pXn", BDTValue_3p0n_vs_3pXn)
        t.SetBranchAddress("TauJetsAuxDyn.PFOEngRelDiff", PFOEngRelDiff)
        if not "nTracks" in names: names.append("nTracks")
        if not "BDTValue_1p0n_vs_1p1n" in names: names.append("BDTValue_1p0n_vs_1p1n")
        if not "BDTValue_1p1n_vs_1pXn" in names: names.append("BDTValue_1p1n_vs_1pXn")
        if not "BDTValue_3p0n_vs_3pXn" in names: names.append("BDTValue_3p0n_vs_3pXn")
        if not "PFOEngRelDiff" in names: names.append("PFOEngRelDiff")
        
        # targets
        truthPtVis  = ROOT.std.vector('double')()
        t.SetBranchAddress("TauJetsAuxDyn.truthPtVis", truthPtVis)
        if not "(target 0) ptTruth" in names: names.append("(target 0) ptTruth")
        if not "(target 1) ptTruth / ptCombined" in names: names.append("(target 1) ptTruth / ptCombined")
        if not "(target 2) (ptTruth - ptPanTau) / (ptLC - ptPanTau)" in names: names.append("(target 2) (ptTruth - ptPanTau) / (ptLC - ptPanTau)")
        
        # other variables for validation / performance evaluation
        ptTauEtaCalib  = ROOT.std.vector('float')()
        pt_combined    = ROOT.std.vector('float')()
        ptFinalCalib   = ROOT.std.vector('float')()
        truthDecayMode = ROOT.std.vector('unsigned long')()
        recoDecayMode  = ROOT.std.vector('int')()
        t.SetBranchAddress("TauJetsAuxDyn.ptTauEtaCalib", ptTauEtaCalib)
        t.SetBranchAddress("TauJetsAuxDyn.pt_combined", pt_combined)
        t.SetBranchAddress("TauJetsAuxDyn.ptFinalCalib", ptFinalCalib)
        t.SetBranchAddress("TauJetsAuxDyn.truthDecayMode", truthDecayMode)
        t.SetBranchAddress("TauJetsAuxDyn.pantau_CellBasedInput_DecayMode", recoDecayMode)
        if not "ptTauEtaCalib" in names: names.append("ptTauEtaCalib")
        if not "pt_combined" in names: names.append("pt_combined")
        if not "ptFinalCalib" in names: names.append("ptFinalCalib")
        if not "truthDecayMode" in names: names.append("truthDecayMode")
        if not "recoDecayMode" in names: names.append("recoDecayMode")
        
        ### tau qualities for selection (won't be written to disk)
        IsTruthMatched = ROOT.std.vector('char')()
        isTauFlags     = ROOT.std.vector('unsigned int')()
        t.SetBranchAddress("TauJetsAuxDyn.IsTruthMatched", IsTruthMatched)
        t.SetBranchAddress("TauJetsAuxDyn.isTauFlags", isTauFlags)
        
        # Loop over tree entries
        for entry in xrange(t.GetEntries()):
            
            t.GetEntry(entry)
            
            for iTau in xrange(truthPtVis.size()):
                if counter % 10000 == 0:
                    print "Selected {} samples out of {} processed samples.".format(counter_selected, counter)
                counter += 1
                if not IsTruthMatched.at(iTau):
                    continue
                if (isTauFlags.at(iTau) / 1048576 % 2):
                    continue  # Medium tau (2^20=1048576) (loose=19, medium=20, tight=21)
                if nTracks.at(iTau) not in [1, 3]:
                    continue
                counter_selected += 1
                ptTruth = truthPtVis.at(iTau) / 1000.
                ptLC = ptDetectorAxis.at(iTau) / 1000.
                ptPanTau = ptPanTauCellBased.at(iTau) / 1000.
                ptCombined = pt_combined.at(iTau) / 1000.
                target0 = ptTruth
                target1 = ptTruth / ptCombined
                target2 = (ptTruth - ptPanTau) / (ptLC - ptPanTau)
                # i.e. ptTruth = x*ptLC + (1-x)*ptPanTau
                tauData = [[# pile-up related
                            mu.at(iTau),
                            nVtxPU.at(iTau),
                            # cluster
                            clusCenterLambda.at(iTau),
                            clusFirstEngDens.at(iTau),
                            clusSecondLambda.at(iTau),
                            clusPresamplerFrac.at(iTau),
                            clusEMProbability.at(iTau),
                            # reconstructed kinematics
                            ptLC, #ptDetectorAxis.at(iTau),
                            ptPanTau,  #ptPanTauCellBased.at(iTau),
                            etaPanTauCellBased.at(iTau),
                            # decay mode related
                            nTracks.at(iTau),
                            BDTValue_1p0n_vs_1p1n.at(iTau),
                            BDTValue_1p1n_vs_1pXn.at(iTau),
                            BDTValue_3p0n_vs_3pXn.at(iTau),
                            PFOEngRelDiff.at(iTau),
                            # target
                            target0, #truthPtVis.at(iTau),
                            target1,
                            target2,
                            # other variables for validation / performance evaluation
                            ptTauEtaCalib.at(iTau) / 1000.,
                            ptCombined,  #pt_combined.at(iTau),
                            ptFinalCalib.at(iTau) / 1000.,
                            truthDecayMode.at(iTau),
                            recoDecayMode.at(iTau),
                           ]]
                data += tauData
                if args.maxsamples != -1 and counter_selected == args.maxsamples:
                    break_all_loops = True
                    break  # break the loop over tree entries
            
            if break_all_loops:
                break  # break the loop over tree entries
        
        f.Close()
        
        if break_all_loops:
            break  # break the loop over input files
    
    print "Selected {} samples out of {} processed samples.".format(counter_selected, counter)
    print "Saving numpy arrays..."
    
    data = np.array(data)
    np_save("{}.data.npy".format(args.output), data)
    
    np.random.seed(42)
    perm = np.random.permutation(len(data))
    np_save("{}.perm.npy".format(args.output), perm)
    
    names = np.array(names)
    np_save("{}.names.npy".format(args.output), names)
    
    print "Done."


#-------------------------------------------------------------------------------
if __name__ == "__main__":

    parser = argparse.ArgumentParser(prog='MxAOD_to_npy')
    parser.add_argument("--input", "-i", default="", type=str, help="The input file name(s). Can be a comma-separated list. Accept * as wild card (remember to enclose quotation marks when using wild card).")
    parser.add_argument("--output", "-o", default="", type=str, help="The output file name.")
    parser.add_argument("--maxsamples", "-m", default=-1, type=int, help="Maximum number of samples to be processed.")
    parser.add_argument("--debug", default=False, action='store_true', help="Debug mode.")
    args = parser.parse_args(sys.argv[1:])
    
    MxAOD_to_npy(args)
