#!/usr/bin/env python

epochs = [50, 100, 200, 400, 800]
layers = [1, 2, 3, 4]
nodes = [16, 24, 32, 48]

for E in epochs:
  for L in layers:
    for N in nodes:
      f = open("/user/wschan/qsub/NN_TES/NN_E{0}_L{1}_N{2}.sh".format(E, L, N), "w")
      f.write("#!/bin/sh\n")
      f.write("\n")
      f.write("source /project/atlas/nikhef/cvmfs/setup.sh\n")
      f.write("source $ATLAS_LOCAL_ROOT_BASE/user/atlasLocalSetup.sh\n")
      f.write("source /cvmfs/sft.cern.ch/lcg/views/setupViews.sh LCG_91 x86_64-centos7-gcc62-opt\n")
      #f.write("export PYTHONPATH=$HOME/local/tensorflow/python2.7/site-packages:$PYTHONPATH\n")
      f.write("\n")
      f.write("cd ~/MVA_TES/NN_TES\n")
      f.write("\n")
      f.write("./train_NN_TES.py -i input_npy/StreamMain_v21_TESTraining_v1 -o output_npy/NN_TES_v1_E{0}_L{1}_N{2} -n test -E {0} -L {1} -N {2}\n".format(E, L, N))
      f.write("\n")
      f.close()
