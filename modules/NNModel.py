#!/usr/bin/env python

#from keras.models import Sequential
from keras.models import Model
from keras.layers import Dense, Input

from modules.utils import model_save
from custom_objects import get_activations, get_objectives


#-------------------------------------------------------------------------------
class NNModel(object):
    def __init__(self, nInputs, nodes=[10,10], epochs=200, batch_size=2*1024, actv='relu', output_actv='sigmoid', loss='MSE'):
        self.nInputs = nInputs
        self.nodes = nodes
        self.layers = len(nodes)
        
        self.epochs = epochs
        self.batch_size = batch_size
        
        self.input_layer = None
        self.hidden_layers = None
        self.output_layer = None
        self.model = None
        
        self.history = None
        
        try:
            self.actv = get_activations(actv)
            self.output_actv = get_activations(output_actv)
        except:
            raise ValueError("NNModel.__init__(): activation function not defined in keras.activations or modules.custom_objects.")
        
        try:
            self.loss = get_objectives(loss)
        except:
            raise ValueError("NNModel.__init__(): loss function not defined in keras.objectives or modules.custom_objects.")
    
    def compile(self):
        self.input_layer = Input(shape = (self.nInputs,))
        
        self.hidden_layers = [Dense(self.nodes[0], activation=self.actv)(self.input_layer)]
        for i,n in enumerate(self.nodes[1:]):
            self.hidden_layers.append(Dense(n, activation=self.actv)(self.hidden_layers[i]))
        
        self.output_layer = Dense(1, activation=self.output_actv)(self.hidden_layers[-1])
        
        model = Model(input=self.input_layer, output=self.output_layer)
        model.compile(loss=self.loss, optimizer='adam')
        print model.summary()
        
        self.model = model
    
    def fit(self, input, target, sample_weight=None, validation_data=None):
        if self.model is None:
            raise ValueError("NNModel.fit(): Model was not compiled. Call NNModel.compile() before training.")
        
        self.history = self.model.fit(input, target, sample_weight=sample_weight, validation_data=validation_data, nb_epoch=self.epochs, batch_size=self.batch_size, verbose=2)
        
        return self.history
    
    def predict(self, input):
        return self.model.predict(input)
    
    def save(self, name):
        model_save(name, self.model)
    
    def copy(self):
        return NNModel(self.nInputs, nodes=self.nodes, epochs=self.epochs, batch_size=self.batch_size, actv=self.actv, output_actv=self.output_actv, loss=self.loss)


##-------------------------------------------------------------------------------
## The Keras wrappers require a function as an argument
## Create a class that initialises with NN model parameters,
## and return a compiled model when called
#class NNModel(object):
#    def __init__(self, nInputs, nodes=[10,10], actv='relu', output_actv='sigmoid', loss='MSE'):
#        self.nInputs = nInputs
#        self.nodes = nodes
#        self.layers = len(nodes)
#        
#        try:
#            self.actv = get_activations(actv)
#            self.output_actv = get_activations(output_actv)
#        except:
#            raise ValueError("NNModel.__init__(): activation function not defined in keras.activations or modules.custom_objects.")
#        
#        try:
#            self.loss = get_objectives(loss)
#        except:
#            raise ValueError("NNModel.__init__(): loss function not defined in keras.objectives or modules.custom_objects.")
#    
#    def __call__(self):
#        model = Sequential()
#        
#        # first hidden layer
#        model.add(Dense(self.nodes[0], input_shape=(self.nInputs,), activation=self.actv))
#        # other hidden layer(s)
#        for n in self.nodes[1:]:
#            model.add(Dense(n, activation=self.actv))
#        # output layer
#        model.add(Dense(1, activation=self.output_actv))
#        
#        model.compile(loss=self.loss, optimizer='adam')
#        print model.summary()
#        
#        return model

