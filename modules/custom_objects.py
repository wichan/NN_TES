#!/usr/bin/env python

#######################################################################
### Define custom activation/loss functions,                        ###
### and hack the Keras activations.get and objectives.get functions ###
### such that it can correctly load models with custom objects      ###
#######################################################################

from keras import backend as K
from keras.models import load_model
from keras.activations import get as get_default_activations
from keras.objectives import get as get_default_objectives

from mock import patch


custom_objects = {}

#-------------------------------------------------------------------------------
# Custom activation functions
custom_activations = {}

def leaky_tanh(x):
    return K.tanh(x) + x*0.01
custom_activations['leaky_tanh'] = leaky_tanh

def leaky_tanh_plus_1(x):
    return (K.tanh(x) + x*0.01) + 1
custom_activations['leaky_tanh_plus_1'] = leaky_tanh_plus_1

def leaky_sigmoid(x):
    return K.sigmoid(x) + x*0.01
custom_activations['leaky_sigmoid'] = leaky_sigmoid

custom_objects.update(custom_activations)


#-------------------------------------------------------------------------------
# Custom loss functions
custom_objectives = {}

def mean_squared_relative_error(y_true, y_pred):
    diff = K.square((y_true - y_pred) / K.clip(K.abs(y_true), K.epsilon(), None))
    return K.mean(diff, axis=-1)
custom_objectives['mean_squared_relative_error'] = mean_squared_relative_error

custom_objects.update(custom_objectives)


#-------------------------------------------------------------------------------
# Patch load model function
def get_activations(x):
    if x in custom_activations:
        return custom_activations[x]
    else:
        return get_default_activations(x)

def get_objectives(x):
    if x in custom_objectives:
        return custom_objectives[x]
    else:
        return get_default_objectives(x)

@patch('keras.activations.get', get_activations)
@patch('keras.objectives.get', get_objectives)
def load_custom_model(file):
    return load_model(file, custom_objects=custom_objects)

