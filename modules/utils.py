#!/usr/bin/env python

import numpy as np
import os, sys


#-------------------------------------------------------------------------------
# Force np.save() to remove existing file
def np_save(path, array):
    if os.path.exists(path):
        print "WARNING: Replacing existing file '{}'".format(path)
        os.remove(path)
    np.save(path, array)


#-------------------------------------------------------------------------------
# Load numpy arrays
def np_load(path, suffixes, max=-1):
    if not hasattr(suffixes, '__iter__'):
        suffixes = [suffixes]
        # string doesn't count as iterable in this case (Python 2)
    
    arrays = []
    perm = None
    
    for suffix in suffixes:
        with open("{}.{}.npy".format(path, suffix)) as f:
            array = np.load(f)
            arrays.append(array)
        if suffix == "perm":
            perm = array
    
    if max+1:
        if perm is None:
            raise ValueError("np_load(): You asked me to load arrays with a 'max' length, but did not ask me to load the permutation ('*.perm.npy').")
        for i,array in enumerate(arrays):
            if not array.shape == perm.shape:
                continue
            arrays[i] = array[perm<max]
    
    return arrays[0] if len(arrays)==1 else tuple(arrays)


#-------------------------------------------------------------------------------
# Force KerasRegressor.model.save() to remove existing file
def model_save(path, model):
    if os.path.exists(path):
        print "WARNING: Replacing existing file '{}'".format(path)
        os.remove(path)
    model.save(path)


#-------------------------------------------------------------------------------
# Normalise dataset and save the mean and std
def normalise_dataset(data):
    mean = np.mean(data, axis=0)
    std = np.std(data, axis=0)
    data = (data - mean) / std
    scaling = np.c_[mean, std]
    return data, scaling


#-------------------------------------------------------------------------------
# Formulae to calculate the actual pT prediction from NN output
def actual_prediction(x, pt1=None, pt2=None, target=2):
    if target == 2:
        # x = (ptTruth - ptPanTau) / (ptLC - ptPanTau)
        # pt1 = ptLC
        # pt2 = ptPanTau
        return x*pt1 + (1-x)*pt2
    elif target == 1:
        # x = ptTruth / ptCombined
        # pt1 = ptCombined
        return x*pt1
    elif target == 0:
        # x = ptTruth
        return x
    else:
        raise ValueError("actual_prediction(): Unknown target.")


#-------------------------------------------------------------------------------
# Calculate relative resolution (half of X% inclusion interval) and non-closure (median)
def relative_resolution(prediction, truth, mask=None, inclusion=68):
    if mask is not None:
        prediction = prediction[mask]
        truth = truth[mask]
    rel_err = prediction / truth  # actually this is relative error plus 1
    lower, upper, median = np.percentile(rel_err, [50 - 0.5*inclusion, 50 + 0.5*inclusion, 50])
    return (upper-lower)/2, median-1

