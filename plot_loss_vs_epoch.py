#!/usr/bin/env python

import sys, os, argparse

import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

from modules.utils import np_load


#-------------------------------------------------------------------------------
def plot_loss_vs_epoch(args):

    # Check whether the configuration is OK
    well_configured = True
    if not args.name:
        well_configured = False
        print "ERROR: You did not specify the model name. (Will use this to find the training history.)"
    if not well_configured:
        print "USAGE: plot_loss_vs_epoch.py -n <model_name> [--debug]"
        sys.exit(1)
    
    print "Loading training history 'plots/{0}/{0}_odd.loss.npy'...".format(args.name)
    history_odd = np_load("plots/{0}/{0}_odd".format(args.name), "loss")
    print "Successfully training history 'plots/{0}/{0}_odd.loss.npy' (epochs = {1}).".format(args.name, len(history_odd))
    
    print "Loading training history 'plots/{0}/{0}_even.loss.npy'...".format(args.name)
    history_even = np_load("plots/{0}/{0}_even".format(args.name), "loss")
    print "Successfully training history 'plots/{0}/{0}_even.loss.npy' (epochs = {1}).".format(args.name, len(history_even))
    
    plt.plot(history_odd[:, 0])
    plt.plot(history_odd[:, 1])
    plt.plot(history_even[:, 0])
    plt.plot(history_even[:, 1])
    plt.legend(['training 1', 'testing 1', 'training 2', 'testing 2'])
    plt.xlabel('Epoch', horizontalalignment='right', x=1.0)
    plt.ylabel('Loss', horizontalalignment='right', y=1.0)
    
    if args.xmax:
        plt.xlim(0, args.xmax)
    if args.ymax and not args.logy:
        plt.ylim(0, args.ymax)
    
    if args.logy:
        ax = plt.gca()
        ax.set_yscale("log")
    
    plt.savefig("plots/{0}/{0}_loss.png".format(args.name))
    plt.savefig("plots/{0}/{0}_loss.pdf".format(args.name))
    plt.savefig("plots/{0}/{0}_loss.eps".format(args.name))
    plt.close()
    
    print "Done."


#-------------------------------------------------------------------------------
if __name__ == "__main__":

    parser = argparse.ArgumentParser(prog='plot_loss_vs_epoch')
    
    parser.add_argument("--name", "-n", default="", type=str, help="Model name.")
    parser.add_argument("--xmax", "-x", default=0, type=float, help="x-axis maximum.")
    parser.add_argument("--ymax", "-y", default=0, type=float, help="y-axis maximum.")
    parser.add_argument("--logy", "-L", default=False, action='store_true', help="y-axis in log scale.")
    parser.add_argument("--debug", default=False, action='store_true', help="Debug mode.")
    
    args = parser.parse_args(sys.argv[1:])
    
    plot_loss_vs_epoch(args)
