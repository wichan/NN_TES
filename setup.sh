#!/bin/sh

if [[ `hostname -f` = stbc*.nikhef.nl ]]; then
  source /project/atlas/nikhef/cvmfs/setup.sh
fi
source $ATLAS_LOCAL_ROOT_BASE/user/atlasLocalSetup.sh

source /cvmfs/sft.cern.ch/lcg/views/setupViews.sh LCG_91 x86_64-slc6-gcc62-opt \
&& echo setup LCG environment successful \
|| (echo setup LCG environment failed; exit 1)

if [[ `hostname -f` = stbc*.nikhef.nl ]]; then
  export PYTHONPATH=$HOME/local/tensorflow/python2.7/site-packages:$PYTHONPATH \
  && echo setup Keras/TensorFlow successful \
  || (echo setup Keras/TensorFlow failed; exit 2)
fi
