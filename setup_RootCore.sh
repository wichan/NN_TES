#!/bin/sh

if [[ `hostname -f` = stbc*.nikhef.nl ]]; then
  source /project/atlas/nikhef/cvmfs/setup.sh
fi
source $ATLAS_LOCAL_ROOT_BASE/user/atlasLocalSetup.sh

source /cvmfs/sft.cern.ch/lcg/views/setupViews.sh LCG_91 x86_64-slc6-gcc62-opt \
&& echo setup LCG environment successful \
|| (echo setup LCG environment failed; exit 1)

if [ -d rcSetup ]; then
  cd rcSetup
  rcSetup \
  && echo setup RootCore successful \
  || (echo setup RootCore failed; exit 3)
  cd ..
else
  mkdir rcSetup
  cd rcSetup
  rcSetup Base,2.5.1 \
  && echo setup RootCore successful \
  || (echo setup RootCore failed; exit 3)
  cd ..
fi
