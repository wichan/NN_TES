#!/usr/bin/env python

import sys, os, argparse
from collections import OrderedDict

import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

from modules.utils import np_load, actual_prediction, relative_resolution


#-------------------------------------------------------------------------------
# Calculate resolutions versus pt
def get_res_versus_pt(algs, pt_truth, nbins=10, pt_bins=None, mask=None):
    if mask is not None:
        pt_truth = pt_truth[mask]
        _algs = OrderedDict()
        for alg in algs:
            _algs[alg] = algs[alg][mask]
        algs = _algs
    
    pt = OrderedDict()
    res_vs_pt = OrderedDict()
    nonClosure_vs_pt = OrderedDict()
    
    if pt_bins is None:
        pt_bins = np.percentile(pt_truth, np.linspace(0, 100, num=nbins+1))
    
    for alg,pt_reco in algs.iteritems():
        pt[alg] = []
        res_vs_pt[alg] = []
        nonClosure_vs_pt[alg] = []
        for lo,hi in zip(pt_bins[:-1], pt_bins[1:]):
            pt_mask = (pt_truth > lo) & (pt_truth < hi)
            res, nc = relative_resolution(pt_reco, pt_truth, pt_mask)
            pt[alg].append(np.mean(pt_reco[pt_mask]))
            res_vs_pt[alg].append(res)
            nonClosure_vs_pt[alg].append(nc)
    
    return pt, res_vs_pt, nonClosure_vs_pt


#-------------------------------------------------------------------------------
# Make resolution-vs-pt plot
def plot_res_vs_pt(pt, res_vs_pt, text="", save_name="plot"):
    for i, alg in enumerate(pt):
        plt.plot(pt[alg], res_vs_pt[alg], marker='x')
    
    plt.legend(res_vs_pt.keys())
    plt.xlabel('<pt_calo> [GeV]', horizontalalignment='right', x=1.0)
    plt.ylabel('Resolution (68% inclusion)', horizontalalignment='right', y=1.0)
    
    ax = plt.gca()
    #ax.set_xscale("log")
    ax.set_xlim([0, 1000])
    ax.set_ylim([0, 0.3])
    ax.grid(color='grey', linestyle=':')
    ax.text(100, 0.27, text, size='xx-large')
    
    if not os.path.splitext(save_name)[1]:
        save_name = "{}.pdf".format(save_name)
    plt.savefig(save_name)
    print "Saved file '{}'".format(save_name)
    
    plt.close()


#-------------------------------------------------------------------------------
def test_NN_TES(args):

    # Check whether the configuration is OK
    well_configured = True
    if not args.input:
        well_configured = False
        print "ERROR: You did not provide the NN input file name."
    if not args.output:
        well_configured = False
        print "ERROR: You did not provide the NN output file name."
    if not args.name:
        well_configured = False
        print "ERROR: You did not specify the output plot name."
    if not args.target in [0,1,2]:
        well_configured = False
        print "ERROR: Target should be either 0, 1 or 2."
    if not args.parity in ['all', 'odd', 'even']:
        well_configured = False
        print "ERROR: Unknown parity '{}'. Valid choices are 'all', 'odd' and 'even'.".format(args.parity)
    if not well_configured:
        print "USAGE: test_NN_TES.py -i <NN_input_file> -o <NN_output_file> -n <output_name> [--target <target_enumeration>] [--parity <parity>] [--final] [--debug]"
        sys.exit(1)
    
    print "Loading NN input dataset '{}.data.npy'...".format(args.input)
    input, input_perm, names = np_load(args.input, ("data", "perm", "names"))
    print "Successfully loaded NN input dataset (nSamples = {}).".format(len(input))
    print "Dataset columns:"
    for i,name in enumerate(names): print "  {0:2}: {1}".format(i, name)
    
    print "Loading NN output '{}.output.npy'...".format(args.output)
    output, output_perm = np_load(args.output, ("output", "perm"))
    print "Successfully loaded NN output (nSamples = {}).".format(len(output))
    
    print "Splitting samples according to parity..."
    # Test only half of the samples
    if args.final:
        input_odd = input[input_perm%4==3]
        input_even = input[input_perm%4==2]
        output_odd = output[output_perm%4==3]
        output_even = output[output_perm%4==2]
    else:
        input_odd = input[input_perm%4==1]
        input_even = input[input_perm%4==0]
        output_odd = output[output_perm%4==1]
        output_even = output[output_perm%4==0]
     
    if args.parity == "all":
        input_test = np.append(input_odd, input_even, axis=0)
        output_test = np.append(output_odd, output_even, axis=0)
    elif args.parity == "odd":
        input_test = input_odd
        output_test = output_odd
    elif args.parity == "even":
        input_test = input_even
        output_test = output_even
    else:
        raise ValueError("Unknown parity '{}'".format(args.parity))
    
    print "Getting TES of different algorithms..."
    if args.target == 2: pt_LC = input_test[:, 7]
    if args.target == 1: pt_combined = input_test[:, 19]
    pt_pantau = input_test[:, 8]
    pt_calo = input_test[:, 18]
    pt_MVA = input_test[:, 20]
    pt_truth = input_test[:, 15]
    #x_train = output_test[:, 0]
    x_test = output_test[:, 1]
    
    if args.target == 2:
        #pred_train = actual_prediction(x_train, pt_LC, pt_pantau, target=2)
        pred_test = actual_prediction(x_test, pt_LC, pt_pantau, target=2)
    elif args.target == 1:
        #pred_train = actual_prediction(x_train, pt_combined, target=1)
        pred_test = actual_prediction(x_test, pt_combined, target=1)
    elif args.target == 0:
        #pred_train = actual_prediction(x_train, target=0)
        pred_test = actual_prediction(x_test, target=0)
    
    print "Started plotting..."
    algs = OrderedDict()
    algs["calo"] = pt_calo
    algs["pantau"] = pt_pantau
    algs["BDT"] = pt_MVA
    algs["NN"] = pred_test
    
    pt_bins = np.linspace(np.log(30), np.log(1000), 10)
    pt_bins = np.exp(pt_bins)
    pt_bins = np.insert(pt_bins, 0, 0.)
    
    pt_test, res_vs_pt_test, nonClosure_vs_pt_test = get_res_versus_pt(algs, pt_truth, pt_bins=pt_bins, mask=None)
    plot_res_vs_pt(pt_test, res_vs_pt_test, r"Overall", "plots/{0}/{0}_allModes".format(args.name))
    
    pt_test, res_vs_pt_test, nonClosure_vs_pt_test = get_res_versus_pt(algs, pt_truth, pt_bins=pt_bins, mask=input_test[:, -1]==0)
    plot_res_vs_pt(pt_test, res_vs_pt_test, r"1p0n", "plots/{0}/{0}_1p0n".format(args.name))
    
    pt_test, res_vs_pt_test, nonClosure_vs_pt_test = get_res_versus_pt(algs, pt_truth, pt_bins=pt_bins, mask=input_test[:, -1]==1)
    plot_res_vs_pt(pt_test, res_vs_pt_test, r"1p1n", "plots/{0}/{0}_1p1n".format(args.name))
    
    pt_test, res_vs_pt_test, nonClosure_vs_pt_test = get_res_versus_pt(algs, pt_truth, pt_bins=pt_bins, mask=input_test[:, -1]==2)
    plot_res_vs_pt(pt_test, res_vs_pt_test, r"1pXn", "plots/{0}/{0}_1pXn".format(args.name))
    
    pt_test, res_vs_pt_test, nonClosure_vs_pt_test = get_res_versus_pt(algs, pt_truth, pt_bins=pt_bins, mask=input_test[:, -1]==3)
    plot_res_vs_pt(pt_test, res_vs_pt_test, r"3p0n", "plots/{0}/{0}_3p0n".format(args.name))
    
    pt_test, res_vs_pt_test, nonClosure_vs_pt_test = get_res_versus_pt(algs, pt_truth, pt_bins=pt_bins, mask=input_test[:, -1]==4)
    plot_res_vs_pt(pt_test, res_vs_pt_test, r"3pXn", "plots/{0}/{0}_3pXn".format(args.name))
    
    print "Done."


#-------------------------------------------------------------------------------
if __name__ == "__main__":

    parser = argparse.ArgumentParser(prog='test_NN_TES')
    
    parser.add_argument("--input", "-i", default="", type=str, help="The training/evaluation dataset file path.")
    parser.add_argument("--output", "-o", default="", type=str, help="The NN output file path.")
    parser.add_argument("--name", "-n", default="", type=str, help="The output plot name.")
    
    parser.add_argument("--target", "-t", default=2, type=int, help="The NN target (0, 1 or 2).")
    parser.add_argument("--parity", "-p", default="all", type=str, help="Evaluate on 'odd', 'even' or 'all' samples.")
    parser.add_argument("--final", "-F", default=False, action='store_true', help="Final performance evaluation, instead of parameters optimisation (will use a different half of the dataset).")
    
    parser.add_argument("--debug", default=False, action='store_true', help="Debug mode.")
    
    args = parser.parse_args(sys.argv[1:])
    
    test_NN_TES(args)
