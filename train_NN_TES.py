#!/usr/bin/env python

import sys, os, argparse

import numpy as np

from keras import backend as K
from keras.wrappers.scikit_learn import KerasRegressor

from modules.utils import np_save, np_load
from modules.utils import normalise_dataset
from modules.NNModel import NNModel


#-------------------------------------------------------------------------------
def train_NN_TES(args):

    #------------------------------
    # Check whether the configuration is OK
    well_configured = True
    if not args.input:
        well_configured = False
        print "ERROR: You did not provide the input file name(s)."
    if not args.output:
        well_configured = False
        print "ERROR: You did not provide the output file name."
    if not args.name:
        well_configured = False
        print "ERROR: You did not provide the output model name."
    if not well_configured:
        print "USAGE: train_NN_TES.py -i <input_file> -o <output_file> -n <model_name> [-m <max_num_samples>] [-E num_epochs] [-L num_layers] [-N num_nodes_per_layer] [--debug]"
        sys.exit(1)
    
    #------------------------------
    print "Loading dataset '{}.data.npy'...".format(args.input)
    #data, perm, names = load_dataset(args.input, args.maxsamples)
    data, perm, names = np_load(args.input, ('data','perm','names'), args.maxsamples)
    print "Successfully loaded dataset (nSamples = {}).".format(len(data))
    print "Dataset columns:"
    for i,name in enumerate(names): print "  {0:2}: {1}".format(i, name)
    
    input = data[:, 0:15]
    target = data[:, 15+args.target]
    
    if args.target == 2:
        weight = np.square( data[:, 7] - data[:, 8] )
    else:
        weight = np.ones(len(target))
    if args.weight == "log":
        weight = weight * np.log(data[:, 15])
    elif args.weight == "sqrt":
        weight = weight * np.sqrt(data[:, 15])
    
    input, scaling = normalise_dataset(input)
    
    if not os.path.exists("models"):
        os.makedirs("models")
    if not os.path.exists("models/{}".format(args.name)):
        os.makedirs("models/{}".format(args.name))
    np_save("models/{0}/{0}.scaling.npy".format(args.name), scaling)
    
    #------------------------------
    print "Splitting dataset into odd and even..."
    odd = perm&1==1
    input_odd = input[odd]
    target_odd = target[odd]
    weight_odd = weight[odd]
    even = perm&1==0
    input_even = input[even]
    target_even = target[even]
    weight_even = weight[even]
    print "Successfully split dataset."
    
    #------------------------------
    print "Creating NN models..."
    model_odd = NNModel(input.shape[1], [args.nodes]*args.layers, epochs=args.epochs, batch_size=args.batch, actv=args.actv, output_actv=args.output_actv, loss=args.loss)
    model_odd.compile()
    
    model_even = model_odd.copy()
    model_even.compile()
    print "Successfully created NN models."
    
    #------------------------------
    print "Training NN model (odd)..."
    validation_data = None
    if not args.no_history:
        validation_data = (input[perm%4==0], target[perm%4==0], weight[perm%4==0])
    history_odd = model_odd.fit(input_odd, target_odd, sample_weight=weight_odd, validation_data=validation_data)
    model_odd.save("models/{0}/{0}_odd.h5".format(args.name))
    print "Successfully trained NN model (odd)."
    
    print "Training NN model (even)..."
    validation_data = None
    if not args.no_history:
        validation_data = (input[perm%4==1], target[perm%4==1], weight[perm%4==1])
    history_even = model_even.fit(input_even, target_even, sample_weight=weight_even, validation_data=validation_data)
    model_even.save("models/{0}/{0}_even.h5".format(args.name))
    print "Successfully trained NN model (even)."
    
    #------------------------------
    if not args.no_history:
        print "Making validation plots..."
        
        if not os.path.exists("plots"):
            os.makedirs("plots")
        
        import matplotlib
        matplotlib.use('Agg')  # No display for running in batch/screen
        import matplotlib.pyplot as plt
        
        plt.plot(history_odd.history['loss'])
        plt.plot(history_odd.history['val_loss'])
        plt.plot(history_even.history['loss'])
        plt.plot(history_even.history['val_loss'])
        plt.legend(['training 1', 'testing 1', 'training 2', 'testing 2'])
        plt.xlabel('Epoch', horizontalalignment='right', x=1.0)
        plt.ylabel('Loss', horizontalalignment='right', y=1.0)
        ax = plt.gca()
        ax.set_yscale("log")
        
        if not os.path.exists("plots"):
            os.makedirs("plots")
        if not os.path.exists("plots/{}".format(args.name)):
            os.makedirs("plots/{}".format(args.name))
        plt.savefig("plots/{0}/{0}_loss.png".format(args.name))
        plt.savefig("plots/{0}/{0}_loss.pdf".format(args.name))
        plt.savefig("plots/{0}/{0}_loss.eps".format(args.name))
        plt.close()
        
        history_odd = np.c_[history_odd.history['loss'], history_odd.history['val_loss']]
        np_save("plots/{0}/{0}_odd.loss.npy".format(args.name), history_odd)
        history_even = np.c_[history_even.history['loss'], history_even.history['val_loss']]
        np_save("plots/{0}/{0}_even.loss.npy".format(args.name), history_even)
    
    #------------------------------
    print "Evaluating on dataset (odd)..."
    output_odd = np.c_[model_odd.predict(input_odd), model_even.predict(input_odd), target_odd]
    print "Successfully evaluated on dataset (odd)."
    
    print "Evaluating on dataset (even)..."
    output_even = np.c_[model_even.predict(input_even), model_odd.predict(input_even), target_even]
    print "Successfully evaluated on dataset (even)."
    
    #------------------------------
    print "Saving numpy arrays..."
    
    output = np.append(output_odd, output_even, axis=0)
    np_save("{}.output.npy".format(args.output), output)
    
    perm = np.append(perm[odd], perm[even])
    np_save("{}.perm.npy".format(args.output), perm)
    
    np_save("{}.scaling.npy".format(args.output), scaling)
    
    print "Done."


#-------------------------------------------------------------------------------
if __name__ == "__main__":

    parser = argparse.ArgumentParser(prog='train_NN_TES')
    
    parser.add_argument("--input", "-i", default="", type=str, help="The input file name(s). Can be a comma-separated list. Accept * as wild card.")
    parser.add_argument("--output", "-o", default="", type=str, help="The output file name.")
    parser.add_argument("--name", "-n", default="", type=str, help="The output model name.")
    parser.add_argument("--maxsamples", "-m", default=-1, type=int, help="Maximum number of samples to be processed.")
    
    parser.add_argument("--epochs", "-E", default=200, type=int, help="NN training parameter: number of epoch cycles.")
    parser.add_argument("--layers", "-L", default=2, type=int, help="NN training parameter: number of layers.")
    parser.add_argument("--nodes", "-N", default=16, type=int, help="NN training parameter: number of nodes.")
    parser.add_argument("--batch", "-B", default=2*1024, type=int, help="NN training parameter: batch size.")

    parser.add_argument("--activation", dest="actv", default="relu", type=str, help="Activation for hidden layers.")
    parser.add_argument("--output-activation", dest="output_actv", default="leaky_sigmoid", type=str, help="Activation for output layers.")
    parser.add_argument("--loss", "--objective", default="MSE", type=str, help="Loss function.")
    
    parser.add_argument("--target", default=2, type=int, help="The NN target (0, 1 or 2).")
    parser.add_argument("--weight", default="", type=str, help="Weights: '' - no weight, 'log' - log(pt), 'sqrt' - sqrt(pt).")
    
    parser.add_argument("--no-history", default=False, action='store_true', help="Don't make loss vs epoch plots (faster training per epoch).")
    
    parser.add_argument("--debug", "-D", default=False, action='store_true', help="Debug mode.")
    
    args = parser.parse_args(sys.argv[1:])
    
    train_NN_TES(args)
